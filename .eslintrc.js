module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "standard-with-typescript",
        "plugin:react/recommended"
    ],
    "ignorePatterns": ["node_modules/"],
    "overrides": [
        {
            "env": {
                "node": true
            },
            "files": [
                ".eslintrc.{js,cjs}"
            ],
            "parserOptions": {
                "sourceType": "script"
            }
        }
    ],
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module",
        "project": "./tsconfig.json",
        "strictNullChecks": true
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "@typescript-eslint/quotes": ["off", "single"], // enforce single quotes
        "@typescript-eslint/semi": ["off", "never"], // enforce no semicolons
        "react/no-deprecated": "off" // consider turning this off if you are using a deprecated ReactDOM method
    },
    "settings": {
        "react": {
          "version": "detect" // Automatically detect the React version
        }
      }
}
