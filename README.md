# react-typescript-starter

###### Minimalist React 18 starter template with TypeScript ⚛ without usage of create-react-app.

Configured with:

- Webpack
- ESLint
- Prettier

Code Coverage: [![codecov](https://codecov.io/gl/samuelcmarandi/testcodecov/branch/main/graph/badge.svg?token=4U0LE1JQDV)](https://codecov.io/gl/samuelcmarandi/testcodecov)
