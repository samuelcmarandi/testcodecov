import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import HelloWorld from '../components/HelloWorld'; // Update the path to your component file

describe('<HelloWorld />', () => {
  it('renders Hello World and environmental variables correctly', () => {
    render(<HelloWorld />);

    expect(screen.getByText('Hello World')).toBeInTheDocument();
    expect(screen.getByText('Environmental variables:')).toBeInTheDocument();
    expect(screen.getByText(/process.env.PRODUCTION:/)).toBeInTheDocument();
    expect(screen.getByText('Hello')).toBeInTheDocument();
    expect(screen.getByText(/process.env.NAME:/)).toBeInTheDocument();
    expect(screen.getByText('Name')).toBeInTheDocument();
    expect(screen.getByText(/process.env.VERSION:/)).toBeInTheDocument();
    expect(screen.getByText('Version')).toBeInTheDocument();
  });
});
