import React from 'react';
import Sample from 'components/Sample';

const HelloWorld: React.FC = () => (
  <>
    <h1>Hello World</h1>

    <hr />

    <h3>Environmental variables:</h3>
    <p>
      process.env.PRODUCTION: <b>Hello</b>
    </p>
    <p>
      process.env.NAME: <b>Name</b>
    </p>
    <p>
      process.env.VERSION: <b>Version</b>
    </p>
    <Sample />
  </>
);

export default HelloWorld;
